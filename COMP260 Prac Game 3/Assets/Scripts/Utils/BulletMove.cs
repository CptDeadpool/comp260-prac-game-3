﻿using UnityEngine;
using System.Collections;

public class BulletMove : MonoBehaviour {

    public float speed = 10.0f;
    public Vector3 direction;
    private Rigidbody rigidbody;
    public float despawn;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        rigidbody.velocity = speed * direction;
        despawn = Time.deltaTime;
        if (despawn == 5)
        {
            Destroy(gameObject);
        }
    }
    void OnCollisionEnter(Collision collision)
    {
        // Destroy the bullet
        Destroy(gameObject);
    }
    
}
